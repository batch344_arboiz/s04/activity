import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook() {
        contacts = new ArrayList<>();
    }
    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    // Other methods to manipulate the phonebook, if needed

    // Getter method to retrieve all contacts
    public ArrayList<Contact> getContacts() {
        return contacts;
    }
}

