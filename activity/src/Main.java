import java.util.ArrayList;
import java.util.Arrays;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John Doe", "09400500611","Quezon City");
        Contact contact2 = new Contact("Jane Doe", "09123456789","Makati City");
        phonebook.addContact(contact1);
        phonebook.addContact(contact2);
        System.out.println(phonebook.getContacts().size());
        ArrayList<Contact> allContacts = phonebook.getContacts();

        // Print all contacts
       if(phonebook.getContacts().size() <1){
           System.out.println("No Contacts found! Please add a new contact");
       }else{
           for (Contact contact : allContacts) {
               System.out.println(contact.getName() + " has the registered phone no.:");
               System.out.println(contact.getContactNumber());

               System.out.println(contact.getName() + " has the registered address:");
               System.out.println(contact.getAddress());

           }
       }

    }
}